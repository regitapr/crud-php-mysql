<?php
    include "database.php";

    $nip = $_GET["nip"];
    $getDosen = "SELECT * FROM dosen WHERE nip_dosen='$nip'";
    $resultGet = mysqli_query($conn, $getDosen);
    $data= mysqli_fetch_array($resultGet);
?>
<!doctype html>
<!-- ps: dibuat sendiri oleh Regita -->
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Update Data Dosen</title>
    <style>
      body {
        background-color: lightsteelblue;
      }
    </style>
  </head>
  <body>
      <section>
        <form action="" method="post">
            <div class="container mt-5">
            <a href="tampilan.php?" class="btn btn-secondary"><i class="bi bi-arrow-left-circle"></i>Return to Home</a> 
                <div class="row justify-content-center">
                <div class="col-8 border border-primary m-3 p-3">
                <h3 class="text-center">Update Data Dosen</h3>
                    <div class="mb-3">
                        <label for="foto" class="form-label">Upload foto</label>
                        <input type="file" name="foto"  value="<?php echo "$data[foto_dosen]"?>" class="form-control" id="formFile" required>
                    </div>
                    <div class="mb-3">
                        <label for="nip" class="form-label">NIP</label>
                        <input type="text" name="nip" value="<?php echo "$data[nip_dosen]"?>" class="form-control" id="nip"required>
                    </div>
                    <div class="mb-3">
                        <label for="nama" class="form-label" value="">Nama</label>
                        <input type="text" name="nama"  value="<?php echo "$data[nama_dosen]"?>"class="form-control" id="nama"required>
                    </div>
                    <div class="row mb-3">
                    <div class="col">
                        <label for="prodi" class="form-label">Prodi</label>
                        <input type="text" name="prodi"  value="<?php echo "$data[prodi]"?>"class="form-control" id="prodi"required>
                    </div>
                    <div class="col">
                        <label for="fakultas" class="form-label">Fakultas</label>
                        <input type="text" name="fakultas" value="<?php echo "$data[fakultas]"?>" class="form-control" id="fakultas"required>
                    </div>
                    </div>
                    <button type="submit" name="save" class="btn btn-primary">Update</button>
                    <a href="tampilan.php?#dosen" class="btn btn-danger"></i>Cancel</a> 
                </div>
            </div>       
            </div>       
        </form>
        <?php
        if (isset($_POST['save'])) {
            $foto= $_POST["foto"];
            $nip = $_POST["nip"];
            $nama = $_POST["nama"];
            $prodi = $_POST["prodi"];
            $fakultas = $_POST["fakultas"];

            $updateDosen = "UPDATE dosen 
                            SET foto_dosen='$foto', nip_dosen='$nip', nama_dosen='$nama', prodi='$prodi', fakultas='$fakultas'
                            ";
            $getUpdate = mysqli_query($conn, $updateDosen);

        }
        ?>
      </section>

      
  </body>
</html>

