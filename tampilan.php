<!DOCTYPE html>
<!-- ps: dibuat sendiri oleh Regita -->
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous" />

    <title>SIM Dosen</title>
    <style>
      body {
        background-color: lightsteelblue;
      }
    </style>
  </head>
  <body class="bg">
  <h1 class="text-center mt-4 mb-3 fs-2"><b>Sistem Informasi Penjadwalan Dosen</b></h1> 
  <hr>
  <section id="dosen">
    <div class="container border border-dark mb-3 mt-5 p-5">
      <h5>Data Dosen</h5>
        <table class="table table-bordered table-striped table-hover text-center mt-3">
          <thead class="table-dark justify">
            <tr">
              <th>ID Dosen</th>
              <th>Foto</th>
              <th>NIP</th>
              <th>Nama Dosen</th>
              <th>Program Studi</th>
              <th>Fakultas</th>
              <th>Action</th>
            </tr>
          </thead>
      <?php 
      include "database.php";
          $getDosen = "SELECT id_dosen, foto_dosen, nip_dosen, nama_dosen, prodi, fakultas FROM dosen";
          $resultGet = mysqli_query($conn, $getDosen);

          if(mysqli_num_rows($resultGet) > 0){
            while ($data = mysqli_fetch_array($resultGet)){
              echo "
              <tr>
                <td>$data[id_dosen]</td>
                <td><img src='$data[foto_dosen]' width='140' height='160'/></td>
                <td>$data[nip_dosen]</td>
                <td>$data[nama_dosen]</td>
                <td>$data[prodi]</td>
                <td>$data[fakultas]</td>
                <td>
                <div class='row d-flex'>
                  <div class='col'>
                    <a href='update.php?nip=$data[nip_dosen]' class='btn btn-sm btn-warning'><i class='bi bi-pencil-square'></i>Update</a>
                  </div>
                  <div class='col'>
                    <a href='delete.php?nip=$data[nip_dosen]' class='btn btn-sm btn-danger'><i class='bi bi-trash'></i>Delete</a>
                  </div>
                </div>
                </td>
            </tr>
              ";
            }
          }else {
            echo '
            <tr>
              <td colspan="6">Tidak ada data.</td>
            </tr>
            ';
          }
      ?>
      </table>  
        <a href="add.php?" class="btn btn-primary"><i class="bi bi-plus-circle"></i> Add Data Dosen</a> 
      </div>
      </div>
    </section>

    <section id="kelas">
      <div class="container border border-dark mb-3 mt-5 p-5">
      <h5>Data Kelas</h5>
        <table class="table table-bordered table-striped table-hover text-center mt-3">
          <thead class="table-dark">
            <tr">
              <th>Id Kelas</th>
              <th>Nama Kelas</th>
              <th>Prodi</th>
              <th>Fakultas</th>
              <th>Action</th>
            </tr>
          </thead>
      <?php 
      include "database.php";
          $getKelas = "SELECT id_kelas, nama_kelas, prodi, fakultas FROM kelas";
          $kelastGet = mysqli_query($conn, $getKelas);
  
          if(mysqli_num_rows($kelastGet) > 0){
            while ($data = mysqli_fetch_array($kelastGet)){
              echo "
              <tr>
                <td>$data[id_kelas]</td>
                <td>$data[nama_kelas]</td>
                <td>$data[prodi]</td>
                <td>$data[fakultas]</td>
                <td>
                <div class='row d-flex'>
                  <div class='col'>
                    <a href='updateKelas.php?idkelas=$data[id_kelas]' class='btn btn-sm btn-warning'><i class='bi bi-pencil-square'></i>Update</a>
                  </div>
                  <div class='col'>
                    <a href='delete.php?idkelas=$data[id_kelas]' class='btn btn-sm btn-danger'><i class='bi bi-trash'></i>Delete</a>
                  </div>
                </div>
                </td>
            </tr>
              ";
            }
          }else {
            echo '
            <tr>
              <td colspan="6">Tidak ada data.</td>
            </tr>
            ';
          }
      ?>
        </div>
      </table>  
      <a href="addKelas.php?" class="btn btn-primary"><i class="bi bi-plus-circle"></i> Add Data</a> 
      </div>
    </section>

    <section id="jadwal">
      <div class="container border border-dark mb-3 mt-5 p-5">
      <h5>Jadwal Kelas</h5>
        <table class="table table-bordered table-striped table-hover text-center mt-3">
          <thead class="table-dark">
            <tr">
              <th>Id Jadwal</th>
              <th>Id Dosen</th>
              <th>Id Kelas</th>
              <th>Jadwal</th>
              <th>Mata Kuliah</th>
              <th>Action</th>
            </tr>
          </thead>
      <?php 
      include "database.php";
          $getJadwal = "SELECT id_jadwal, id_dosen, id_kelas, jadwal, mata_kuliah FROM jadwal_kelas";
          $jadwaltGet = mysqli_query($conn, $getJadwal);
  
          if(mysqli_num_rows($jadwaltGet) > 0){
            while ($data = mysqli_fetch_array($jadwaltGet)){
              echo "
              <tr>
                <td>$data[id_jadwal]</td>
                <td>$data[id_dosen]</td>
                <td>$data[id_kelas]</td>
                <td>$data[jadwal]</td>
                <td>$data[mata_kuliah]</td>
                <td>
                <div class='row d-flex'>
                  <div class='col'>
                    <a href='updateJadwal.php?idjadwal=$data[id_jadwal]' class='btn btn-sm btn-warning'><i class='bi bi-pencil-square'></i>Update</a>
                  </div>
                  <div class='col'>
                    <a href='delete.php?idjadwal=$data[id_jadwal]' class='btn btn-sm btn-danger'><i class='bi bi-trash'></i>Delete</a>
                  </div>
                </div>
                </td>
            </tr>
              ";
            }
          }else {
            echo '
            <tr>
              <td colspan="6">Tidak ada data.</td>
            </tr>
            ';
          }
      ?>
        </div>
      </table>  
      <a href="addJadwal.php?" class="btn btn-primary"><i class="bi bi-plus-circle"></i> Add Data</a> 
      </div>
    </section>
  </body>
</html>
