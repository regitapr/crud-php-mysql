<!doctype html>
<!-- ps: dibuat sendiri oleh Regita -->
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Add Kelas</title>
    <style>
      body {
        background-color: lightsteelblue;
      }
    </style>
  </head>
  <body>
    <section id="kelas">
      <form action="" method="post">
        <div class="container mt-5">
        <a href="tampilan.php?" class="btn btn-secondary"><i class="bi bi-arrow-left-circle"></i>Return to Home</a> 
          <div class="row justify-content-center">
            <div class="col-8 border border-primary m-3 p-3">
              <h3 class="text-center">Tambah Data Kelas</h3>
                <div class="mb-3">
                  <label for="id" class="form-label">ID Kelas</label>
                  <input type="text" name="idkelas" class="form-control" id="id" readonly>
                </div>
                <div class="mb-3">
                  <label for="nama" class="form-label">Nama Kelas</label>
                  <input type="text" name="namakelas" class="form-control" id="nama"required>
                </div>
                <div class="row mb-3">
                <div class="col">
                  <label for="prodi" class="form-label">Prodi</label>
                  <input type="text" name="progdi" class="form-control" id="prodi"required>
                </div>
                <div class="col">
                  <label for="fakultas" class="form-label">Fakultas</label>
                  <input type="text" name="fakult" class="form-control" id="fakultas"required>
                </div>
                </div>
                <button type="submit" name="submit" class="btn btn-primary">Add Data</button>
                <a href="tampilan.php?#kelas" class="btn btn-danger"></i>Cancel</a> 
            </div>
          </div> 
        </div> 
      </form>
    <?php
    include "database.php";
    if (isset($_POST['submit'])) {
        $idkelas = $_POST["idkelas"];
        $namakelas = $_POST["namakelas"];
        $progdi = $_POST["progdi"];
        $fakult = $_POST["fakult"];


        $insertKelas = "INSERT INTO kelas(id_kelas, nama_kelas, prodi, fakultas) 
                        VALUES('$idkelas','$namakelas','$progdi','$fakult')";

        $add = mysqli_query($conn, $insertKelas);

        if (isset($insertKelas) > 0) {
        echo "
          <div class='alert alert-success' role='alert'>Simpan data berhasil!<a href='tampilan.php'>Lihat Data</a></div>
        ";
        }else{
        echo "
          <div class='alert alert-danger' role='alert'>Simpan data gagal! <a href='tampilan.php'>Lihat Data</a>
          </div>
        ";
        }
    }
    
    ?>
    </section>

  </body>
</html>