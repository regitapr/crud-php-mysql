<!doctype html>
<!-- ps: dibuat sendiri oleh Regita -->
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Add Data Dosen</title>
    <style>
      body {
        background-color: lightsteelblue;
      }
    </style>
  </head>
  <body>
    <section id="dosen">
      <form action="" method="post">
          <div class="container mt-5">
          <a href="tampilan.php?" class="btn btn-secondary"><i class="bi bi-arrow-left-circle"></i>Return to Home</a> 
              <div class="row justify-content-center">
              <div class="col-8 border border-primary m-3 p-3">
              <h3 class="text-center">Add Data Dosen</h3>
                <div class="mb-3">
                  <label for="foto" class="form-label">Upload foto</label>
                  <input class="form-control" name="foto" type="file" id="formFile" required>
                </div>
                <div class="mb-3">
                  <label for="nip" class="form-label">NIP</label>
                  <input type="text" name="nip" class="form-control" id="nip"required>
                </div>
                <div class="mb-3">
                  <label for="nama" class="form-label">Nama</label>
                  <input type="text" name="nama" class="form-control" id="nama"required>
                </div>
                <div class="row mb-3">
                  <div class="col">
                    <label for="prodi" class="form-label">Prodi</label>
                    <input type="text" name="prodi" class="form-control" id="prodi"required>
                  </div>
                  <div class="col">
                    <label for="fakultas" class="form-label">Fakultas</label>
                    <input type="text" name="fakultas" class="form-control" id="fakultas"required>
                  </div>
                </div>
                <button type="submit" name="save" class="btn btn-primary">Add Data</button>
                  <a href="tampilan.php?#dosen" class="btn btn-danger"></i>Cancel</a> 
              </div>
            </div>
          </div>       
        </form>
      <?php
      include "database.php";
      if (isset($_POST['save'])) {
          $foto= $_POST["foto"];
          $nip = $_POST["nip"];
          $nama = $_POST["nama"];
          $prodi = $_POST["prodi"];
          $fakultas = $_POST["fakultas"];

          $getDosen = "SELECT * FROM dosen WHERE nip_dosen= $nip";
          $resultGet= mysqli_query($conn, $getDosen);
          $cek = mysqli_num_rows($resultGet);

          $add = $_GET['nip'];
          $insertDosen = "INSERT INTO dosen(foto_dosen, nip_dosen, nama_dosen, prodi, fakultas) 
                          VALUES('$foto','$nip','$nama','$prodi','$fakultas')";

          $insert = mysqli_query($conn, $insertDosen);

          if (isset($insertDosen) && $cek  <= 0) {
          echo "
            <div class='alert alert-success' role='alert'>Simpan data berhasil!<a href='tampilan.php'>Lihat Data</a></div>
          ";
          }else if ($cek > 0) {
          echo "
            <div class='alert alert-danger' role='alert'>Simpan data gagal! <a href='tampilan.php'>Lihat Data</a>
            </div>
          ";
          }
      }
      ?>
    </section>
  </body>
</html>

