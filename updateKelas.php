<?php
    include "database.php";

    $idkelas = $_GET["idkelas"];
    $getKelas = "SELECT * FROM kelas WHERE id_kelas='$idkelas'";
    $kelasGet = mysqli_query($conn, $getKelas);
    $data= mysqli_fetch_array($kelasGet);
?>
<!doctype html>
<html lang="en">
    <!-- ps: dibuat sendiri oleh Regita -->
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Update Data Kelas</title>
    <style>
      body {
        background-color: lightsteelblue;
      }
    </style>
  </head>
  <body>
      <section>
        <form action="" method="post">
            <div class="container mt-5">
            <a href="tampilan.php?" class="btn btn-secondary"><i class="bi bi-arrow-left-circle"></i>Return to Home</a> 
                <div class="row justify-content-center">
                <div class="col-8 border border-primary m-3 p-3">
                <h3 class="text-center">Update Data Kelas</h3>
                <div class="mb-3">
                  <label for="id" class="form-label">ID Kelas</label>
                  <input type="text" name="idkelas"  value="<?php echo "$data[id_kelas]"?>" class="form-control" id="id" readonly>
                </div>
                <div class="mb-3">
                  <label for="nama" class="form-label">Nama Kelas</label>
                  <input type="text" name="namakelas"  value="<?php echo "$data[nama_kelas]"?>" class="form-control" id="nama" required>
                </div>
                <div class="row mb-3">
                <div class="col">
                  <label for="prodi" class="form-label">Prodi</label>
                  <input type="text" name="progdi"  value="<?php echo "$data[prodi]"?>" class="form-control" id="prodi"required>
                </div>
                <div class="col">
                  <label for="fakultas" class="form-label">Fakultas</label>
                  <input type="text" name="fakult"  value="<?php echo "$data[fakultas]"?>" class="form-control" id="fakultas"required>
                </div>
                </div>
                <button type="submit" name="update" class="btn btn-primary">Update</button>
                <a href="tampilan.php?#kelas" class="btn btn-danger"></i>Cancel</a> 
            </div>
                </div>
            </div>       
            </div>       
        </form>
        <?php
        if (isset($_POST['update'])) {
            $idkelas= $_POST["idkelas"];
            $namakelas = $_POST["namakelas"];
            $progdi = $_POST["progdi"];
            $fakult = $_POST["fakult"];

            $upKelas = "UPDATE kelas 
                            SET id_kelas='$idkelas', nama_kelas='$namakelas', prodi='$progdi', fakultas='$fakult'
                            ";
            $klsUpdate = mysqli_query($conn, $upKelas);

        }
        ?>
      </section>       
  </body>
</html>
