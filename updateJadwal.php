<?php
    include "database.php";

    $idjadwal = $_GET["idjadwal"];
    $getJad = "SELECT * FROM jadwal_kelas WHERE id_jadwal='$idjadwal'";
    $jadGet = mysqli_query($conn, $getJad);
    $data= mysqli_fetch_array($jadGet);
?>
<!doctype html>
<!-- ps: dibuat sendiri oleh Regita -->
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Update Jadwal</title>
    <style>
      body {
        background-color: lightsteelblue;
      }
    </style>
  </head>
  <body>
      <section>
        <form action="" method="post">
            <div class="container mt-5">
            <a href="tampilan.php?" class="btn btn-secondary"><i class="bi bi-arrow-left-circle"></i>Return to Home</a> 
                <div class="row justify-content-center">
                <div class="col-8 border border-primary m-3 p-3">
                <h3 class="text-center">Update Jadwal</h3>
                <div class="mb-3">
                  <label for="idjadwal" class="form-label">ID Jadwal</label>
                  <input type="text" name="idjadwal"  value="<?php echo "$data[id_jadwal]"?>" class="form-control" id="idjadwal" readonly>
                </div>
                <div class="mb-3">
                  <label for="idkelas" class="form-label">ID Kelas</label>
                  <input type="text" name="idkelas"  value="<?php echo "$data[id_kelas]"?>" class="form-control" id="idkelas" required>
                </div>
                <div class="mb-3">
                  <label for="id" class="form-label">ID Dosen</label>
                  <input type="text" name="id"  value="<?php echo "$data[id_dosen]"?>" class="form-control" id="id" required>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <label for="jadwal" class="form-label">Jadwal</label>
                        <input type="datetime-local" name="jadwal" value="<?php echo "$data[jadwal]"?>" class="form-control" id="jadwal"required>   
                    </div>
                    <div class="col">
                        <label for="matkul" class="form-label">Mata Kuliah</label>
                        <input type="text" name="matkul" value="<?php echo "$data[mata_kuliah]"?>" class="form-control" id="matkul"required>
                    </div>
                </div>
                <button type="submit" name="update" class="btn btn-primary">Update</button>
                <a href="tampilan.php?#kelas" class="btn btn-danger"></i>Cancel</a> 
            </div>
                </div>
            </div>       
            </div>       
        </form>
        <?php
        if (isset($_POST['update'])) {
            $idjadwal= $_POST["idjadwal"];
            $idkelas= $_POST["idkelas"];
            $id= $_POST["id"];
            $jadwal = $_POST["jadwal"];
            $matkul = $_POST["matkul"];

            $upJadwal = "UPDATE jadwal_kelas 
                         SET id_jadwal'$idjadwal', id_kelas='$idkelas',id_dosen='$id', jadwal='$jadwal', mata_kuliah='$matkul'
                         ";
            $jadUpdate = mysqli_query($conn, $upJadwal);

        }
        ?>
      </section>       
  </body>
</html>
