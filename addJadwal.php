<!doctype html>
<!-- ps: dibuat sendiri oleh Regita -->
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Add Jadwal</title>
    <style>
      body {
        background-color: lightsteelblue;
      }
    </style>
  </head>
  <body>
    <section id="kelas">
      <form action="" method="post">
        <div class="container mt-5">
        <a href="tampilan.php?" class="btn btn-secondary"><i class="bi bi-arrow-left-circle"></i>Return to Home</a> 
          <div class="row justify-content-center">
            <div class="col-8 border border-primary m-3 p-3">
              <h3 class="text-center">Tambah Data Jadwal</h3>
                <div class="mb-3">
                  <label for="idjadwal" class="form-label">ID Jadwal</label>
                  <input type="text" name="idjadwal" class="form-control" id="idjadwal" readonly>
                </div>
                <div class="mb-3">
                  <label for="idkelas" class="form-label">ID Kelas</label>
                  <input type="text" name="idkelas" class="form-control" id="id" required>
                </div>
                <div class="mb-3">
                  <label for="id" class="form-label">ID Dosen</label>
                  <input type="text" name="id" class="form-control" id="id" required>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <label for="jadwal" class="form-label">Jadwal</label>
                        <input type="datetime-local" name="jadwal" class="form-control" id="jadwal"required>   
                    </div>
                    <div class="col">
                        <label for="matkul" class="form-label">Mata Kuliah</label>
                        <input type="text" name="matkul" class="form-control" id="matkul"required>
                    </div>
                </div>
                <button type="submit" name="simpan" class="btn btn-primary">Add Data</button>
                <a href="tampilan.php?#jadwal" class="btn btn-danger"></i>Cancel</a> 
            </div>
          </div> 
        </div> 
      </form>
    <?php
    include "database.php";
    if (isset($_POST['simpan'])) {
        $idjadwal = $_POST["idjadwal"];
        $idkelas = $_POST["idkelas"];
        $id = $_POST["id"];
        $jadwal = $_POST["jadwal"];
        $matkul = $_POST["matkul"];

        $getJad= "SELECT * FROM jadwal_kelas WHERE id_jadwal= $_POST[idjadwal]";
        $jadGet= mysqli_query($conn, $getJad);
        $tes = mysqli_num_rows($jadGet);

        $insertJad = "INSERT INTO jadwal_kelas(id_jadwal, id_kelas, id_dosen, jadwal, mata_kuliah) 
                        VALUES('$idjadwal','$idkelas','$id','$jadwal','$matkul')";

        $add = mysqli_query($conn, $insertJad);

        if (isset($insertJad) && $tes  <= 0) {
        echo "
          <div class='alert alert-success' role='alert'>Simpan data berhasil!<a href='tampilan.php'>Lihat Data</a></div>
        ";
        }else if ($tes > 0) {
        echo "
          <div class='alert alert-danger' role='alert'>Simpan data gagal! <a href='tampilan.php'>Lihat Data</a>
          </div>
        ";
        }
    }
    ?>
    </section>
    
  </body>
</html>